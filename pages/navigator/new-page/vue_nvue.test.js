jest.setTimeout(30000)

const INTERMEDIATE_PAGE_PATH = "/pages/navigator/navigator";
const NEW_PAGE1_PATH = "/pages/navigator/new-page/new-page-1";
const NEW_PAGE2_PATH = "/pages/navigator/new-page/new-page-2";
const NEW_PAGE3_PATH = "/pages/navigator/new-page/new-page-3";
let page;

describe("vue & nvue", () => {
  it("路由跳转", async () => {
    page = await program.reLaunch(INTERMEDIATE_PAGE_PATH);
    await page.waitFor("view");
    expect(page.path).toBe(INTERMEDIATE_PAGE_PATH.substring(1))
    
    page = await program.navigateTo(NEW_PAGE1_PATH)
    await page.waitFor("view");
    expect(page.path).toBe(NEW_PAGE1_PATH.substring(1))
    
    page = await program.navigateTo(NEW_PAGE2_PATH)
    await page.waitFor("view");
    expect(page.path).toBe(NEW_PAGE2_PATH.substring(1))
    
    page = await program.navigateTo(NEW_PAGE3_PATH)
    await page.waitFor(1000);
    expect(page.path).toBe(NEW_PAGE3_PATH.substring(1))
    
    page = await program.navigateBack()
    await page.waitFor("view");
    expect(page.path).toBe(NEW_PAGE2_PATH.substring(1))
    
    page = await program.navigateBack()
    await page.waitFor("view");
    expect(page.path).toBe(NEW_PAGE1_PATH.substring(1))
    
    page = await program.navigateBack()
    await page.waitFor("view");
    expect(page.path).toBe(INTERMEDIATE_PAGE_PATH.substring(1))
  });
});
