import {reactive} from 'vue'

export const state = reactive({ lifeCycleNum: 0 })

export const setLifeCycleNum = (num) => {
  state.lifeCycleNum = num
}
